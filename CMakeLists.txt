project(specmicp-princetonpy)
cmake_minimum_required(VERSION 2.8.8)

# Flags
# ======
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -pedantic")
message(STATUS "c++ flags : ${CMAKE_CXX_FLAGS}")

# External modules
# ================
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)

find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
find_package(Eigen3 REQUIRED)  # This module comes from the Eigen3 Package
include_directories(${EIGEN3_INCLUDE_DIR})

include_directories("cpp_src")
include_directories("cpp_src/3rdparty/jsoncpp")

option(SPECMICP_PYTHON_VERSION_3 "Version of python for cython compilation" ON)

if (SPECMICP_PYTHON_VERSION_3)
    find_package(PythonInterp 3 REQUIRED)
    find_package(PythonLibs 3 REQUIRED)
else()
    find_package(PythonInterp 2.7 REQUIRED)
    find_package(PythonLibs 2.7 REQUIRED)
endif()

# The CPP module
# ==============
set(DATABASE_DIR "cpp_src/database")
set(PHYSICS_DIR "cpp_src/physics")
set(UTILS_DIR "cpp_src/utils")

add_custom_target(database_incl
    SOURCES
    ${DATABASE_DIR}/species/ionic_parameters.hpp
    ${DATABASE_DIR}/species/component_list.hpp
    ${DATABASE_DIR}/species/aqueous_list.hpp

    ${DATABASE_DIR}/section_name.hpp

    ${DATABASE_DIR}/errors.hpp
    ${DATABASE_DIR}/module.hpp
    
    ${UTILS_DIR}/log.hpp
)

set(DATABASE_LIBFILE
    ${DATABASE_DIR}/species/species.cpp
    ${DATABASE_DIR}/species/base_wrapper.cpp
    ${DATABASE_DIR}/species/mineral_list.cpp
    ${DATABASE_DIR}/species/gas_list.cpp
    ${DATABASE_DIR}/species/sorbed_list.cpp
    ${DATABASE_DIR}/species/compounds_list.cpp


    ${DATABASE_DIR}/data_container.cpp
    ${DATABASE_DIR}/module.cpp
    ${DATABASE_DIR}/database.cpp
    ${DATABASE_DIR}/reader.cpp
    ${DATABASE_DIR}/selector.cpp
    ${DATABASE_DIR}/appender.cpp
    ${DATABASE_DIR}/mineral_selector.cpp
    ${DATABASE_DIR}/aqueous_selector.cpp
    ${DATABASE_DIR}/oxydo_selector.cpp
    ${DATABASE_DIR}/switch_basis.cpp
    ${DATABASE_DIR}/writer.cpp

    ${PHYSICS_DIR}/units.cpp
    
    ${UTILS_DIR}/dateandtime.cpp
    ${UTILS_DIR}/json.cpp
    
    cpp_src/3rdparty/jsoncpp/jsoncpp.cpp
)

add_library(specmicp_database STATIC ${DATABASE_LIBFILE})
# To be compatible with python the target must be build with -fPIC (position independent code)
set_property(TARGET specmicp_database PROPERTY POSITION_INDEPENDENT_CODE 1)

# The python module
# ==================
set(CYTHON_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/cython_src)
configure_file(setup.py.in ${CMAKE_CURRENT_BINARY_DIR}/setup.py @ONLY)

add_custom_target(cython ALL
  COMMAND ${PYTHON_EXECUTABLE} setup.py build
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  DEPENDS specmicp_database
  COMMENT "Build the cython module"
  )
add_custom_target(cython_2 ALL
  COMMAND ${PYTHON_EXECUTABLE} setup.py install --install-lib=.
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  DEPENDS cython
  COMMENT "Build the cython module"
  )
  
  set (DATABASE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/data)
  
  configure_file(scripts/test_database.py
       ${CMAKE_CURRENT_BINARY_DIR}/test_database.py
       @ONLY
       )