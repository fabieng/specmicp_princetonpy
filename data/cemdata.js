// This is a manual and partial translation of the CEMDATA14 database based on the PSI/Nagra databse (see references below)
// The list of references is at the end of the database
// The database is in the JSON format, to be read easily by both human and machine.
//
// Warning : The C-S-H model is from the CEMDATA2007 database
// (i.e. : Jennite/tobermorite)
{
    "Metadata": {
        "name": "CEMDATA14",
        "version": "0.2.0"
    },
    "Basis": [
        {
            "label": "H2O",
            "molar_mass": 18.015 // g/mol
        },
        {
            "label": "E[-]",
            "molar_mass": 0.00
        },
        {
            "label": "H[+]",
            "molar_mass": 1.008,
            "activity": {
                "a": 9.00,
                "b": 0.00
            }
        },
        {
            "label": "Al[3+]",
            "molar_mass": 26.982,
            "activity": {
                "a": 6.65,
                "b": 0.19
            }
        },
        {
            "label": "HCO3[-]",
            "molar_mass": 61.016,
            "activity": {
                "a": 5.40,
                "b": 0.00
            }
        },
        {
            "label": "Ca[2+]",
            "molar_mass": 40.078,
            "activity": {
                "a": 4.86,
                "b": 0.15
            }
        },
        {
            "label": "Fe[2+]",
            "molar_mass": 55.845,
            "activity": {
                "a": 5.08,
                "b": 0.16
            }
        },
        {
            "label": "Cl[-]",
            "molar_mass": 35.453,
            "activity": {
                "a": 3.71,
                "b": 0.01
            }
        },
        {
            "label": "Na[+]",
            "molar_mass": 22.99,
            "activity": {
                "a": 4.32,
                "b": 0.06
            }
        },
        {
            "label": "Si(OH)4",
            "molar_mass": 96.114,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "SO4[2-]",
            "molar_mass": 96.063,
            "activity": {
                "a": 5.31,
                "b": -0.07
            }
        },
        {
            "label": "K[+]",
            "molar_mass": 39.098,
            "activity": {
                "a": 3.71,
                "b": 0.0
            }
        },
        {
            "label": "NO3[-]",
            "molar_mass": 62.004,
            "activity": {
                "a": 3.00,
                "b": 0.0
            }

        },
        {
            "label": "Mg[2+]",
            "molar_mass": 24.305,
            "activity": {
                "a": 5.46,
                "b": 0.22
            }
        }

    ],
    "Aqueous": [
        {
            "label": "HO[-]",
            "composition": "H2O, - H[+]",
            "log_k": 13.9995,
            "activity": {
                "a": 10.65,
                "b": 0.00
            }
        },
        {
            "label": "AlOH[2+]",
            "composition": "Al[3+], H2O, -H[+]",
            "log_k": 4.9573,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Al(OH)2[+]",
            "composition": "Al[3+], 2 H2O, -2 H[+]",
            "log_k": 10.5943,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Al(OH)3",
            "composition": "Al[3+], 3 H2O, -3 H[+]",
            "log_k": 16.4328,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "Al(OH)4[-]",
            "composition": "Al[3+], 4 H2O, -4 H[+]",
            "log_k": 22.8797,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "AlSO4[+]",
            "composition": "Al[3+], SO4[2-]",
            "log_k": -3.900,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Al(SO4)2[-]",
            "composition": "Al[3+], 2 SO4[2-]",
            "log_k": -5.9000,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "HSO4[-]",
            "composition": "SO4[2-], H[+]",
            "log_k": -1.9878,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "CO2",
            "composition": "-H2O, H[+], +HCO3[-]",
            "log_k": -6.3519,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "CO3[2-]",
            "composition": "HCO3[-],- H[+]",
            "log_k": 10.3289,
            "activity": {
                "a": 5.40,
                "b": 0.00
            }
        },
        {
            "label": "CaSO4",
            "composition": "Ca[2+], SO4[2-]",
            "log_k": -2.300,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "SiO(OH)3[-]",
            "composition": "Si(OH)4, -H[+]",
            "log_k": 9.8100,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "SiO2(OH)2[2-]",
            "composition": "Si(OH)4, -2 H[+]",
            "log_k": 23.1400,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "AlSiO(OH)3[2+]",
            "composition": "Al[3+], SiO(OH)3[-]",
            "log_k": -7.400,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Al(OH)6SiO[-]",
            "composition": "Al(OH)4[-], Si(OH)4, -H2O",
            "log_k": -3.600,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "CaCO3",
            "composition": "HCO3[-], Ca[2+], -H[+]",
            "log_k": 7.1048,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "CaHCO3[+]",
            "composition": "HCO3[-], Ca[2+]",
            "log_k": -1.1057,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "CaOH[+]",
            "composition": "Ca[2+], H2O, -H[+]",
            "log_k": 12.7800,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "CaSiO(OH)3[+]",
            "composition": "Ca[2+], SiO(OH)3[-]",
            "log_k": -1.200,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "CaSiO2(OH)2",
            "composition": "Ca[2+], SiO2(OH)2[2-]",
            "log_k": -4.60,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "KOH",
            "composition": "K[+], H2O, -H[+]",
            "log_k": 14.460,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "KSO4[-]",
            "composition": "K[+], SO4[2-]",
            "log_k": -0.85,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "NaCO3[-]",
            "composition": "Na[+], -H[+], HCO3[-]",
            "log_k": 9.0590,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "NaHCO3",
            "composition": "Na[+], HCO3[-]",
            "log_k": 0.2500,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "NaOH",
            "composition": "Na[+], H2O, -H[+]",
            "log_k": 14.18,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "NaSO4[-]",
            "composition": "Na[+], SO4[2-]",
            "log_k": -0.7,
            "activity:": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "HSO4[-]",
            "composition": "H[+], SO4[2-]",
            "log_k": -1.9878,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "N2(aq)",
            "composition": "2.0 NO3[-], 12.0 H[+], -6.0 H2O, +10.0 E[-]",
            "log_k": -207.2676,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "NH4[+]",
            "composition": "NO3[-], 10.0H[+], -3.0 H2O, +8.0 E[-]",
            "log_k": 119.1372,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }

        },
        {
            "label": "NH3",
            "composition": "NH4[+], -H[+]",
            "log_k": 9.2370,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "O2(aq)",
            "composition": "2 H2O, -4.0 H[+], -4 E[-]",
            "log_k": 85.9862,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "H2(aq)",
            "composition": "2 H[+], 2 E[-]",
            "log_k": 3.105,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "Fe[3+]",
            "composition": "Fe[2+], -E[-]",
            "log_k": 13.02,
            "activity": {
                "a": 9.00,
                "b": 0.00
            }
        },
        {
            "label": "Fe(OH)2[+]",
            "composition": "Fe[3+], -2.00 H[+], +2.00 H2O",
            "log_k": 5.6700,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Fe(OH)3",
            "composition": "Fe[3+], -3.00 H[+], +3.00 H2O",
            "log_k": 12.5600,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "Fe(OH)4[-]",
            "composition": "Fe[3+], -4.00 H[+], +4.00 H2O",
            "log_k": 21.6000,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Fe2(OH)2[4+]",
            "composition": "2.00 Fe[3+], -2.00 H[+], +2.00 H2O",
            "log_k": 2.9500,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Fe3(OH)4[5+]",
            "composition": "3.00 Fe[3+], -4.00 H[+], +4.00 H2O",
            "log_k": 6.3000,
            "activity": {
                "a": 9.00,
                "b": 0.00
            }
        },
        {
            "label":  "FeOH[+]",
            "composition": "Fe[2+], H2O, - H[+]",
            "log_k": 9.5000,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label":  "FeOH[2+]",
            "composition": "Fe[3+], H2O, - H[+]",
            "log_k": 2.1900,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "Fe(SO4)2[-]",
            "composition": "Fe[3+], 2 SO4[2-]",
            "log_k": -5.3800,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeHSO4[2+]",
            "composition": "Fe[3+], H[+],  SO4[2-]",
            "log_k": -4.4680,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeHSO4[+]",
            "composition": "Fe[2+], H[+],  SO4[2-]",
            "log_k": -3.0680,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeSO4[+]",
            "composition": "Fe[3+],  SO4[2-]",
            "log_k": -4.0400,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeSO4",
            "composition": "Fe[2+],  SO4[2-]",
            "log_k": -2.2500,
            "activity": {
                "a": 0.00,
                "b": 0.10
           }
        },
        {
            "label": "FeCl[+]",
            "composition": "Fe[2+], Cl[-]",
            "log_k": -0.14,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeCl[2+]",
            "composition": "Fe[3+], Cl[-]",
            "log_k": -1.48,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeCl2[+]",
            "composition": "Fe[3+], 2.00 Cl[-]",
            "log_k": -2.13,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "FeCl3",
            "composition": "Fe[3+], 3.00 Cl[-]",
            "log_k": -1.1300,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "FeCO3",
            "composition": "Fe[2+], HCO3[-], -H[+]",
            "log_k": 5.9490,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "FeSiO(OH)3[2+]",
            "composition": "Fe[3+], SiO(OH)3[-]",
            "log_k": -9.70,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "HS[-]",
            "composition": "SO4[2-], 9.00 H[+], 8 E[-], -4 H2O",
            "log_k": -33.6900,
            "activity": {
                "a": 3.50,
                "b": 0.00
            }
        },
        {
            "label": "S[2-]",
            "composition": "HS[-], - H[+]",
            "log_k": 19.00,
            "activity": {
                "a": 5.00,
                "b": 0.00
            }
        },
        {
            "label": "H2S(aq)",
            "composition": "HS[-], H[+]",
            "log_k": -6.9900,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "S2O3[2-]",
            "composition": "2.00 SO4[2-], 10.00 H[+], 8.00 E[-], -5.00 H2O",
            "log_k": -38.0145,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "MgCO3",
            "composition": "Mg[2+], - H[+], HCO3[-]",
            "log_k": 7.3492,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "MgHCO3[+]",
            "composition": "Mg[2+], HCO3[-]",
            "log_k": -1.0682,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "MgOH[+]",
            "composition": "Mg[2+], HO[-]",
            "log_k": 11.4400,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "MgSO4",
            "composition": "Mg[2+], SO4[2-]",
            "log_k": -2.3700,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        },
        {
            "label": "MgSiO(OH)3[+]",
            "composition": "Mg[2+], SiO(OH)3[-]",
            "log_k": -1.500,
            "activity": {
                "a": 4.00,
                "b": 0.00
            }
        },
        {
            "label": "MgSiO2(OH)2",
            "composition": "Mg[2+], SiO2(OH)2[2-]",
            "log_k": -5.700,
            "activity": {
                "a": 0.00,
                "b": 0.10
            }
        }
    ],
    "Minerals": [
    //cemdata14
    {
        "label": "Ettringite",
        "composition": "6 Ca[2+], 2 Al(OH)4[-], 3 SO4[2-], 4 HO[-], 26 H2O",
        "log_k": -44.9,
        "molar_volume": 707,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Tricarboaluminate",
        "composition": "6 Ca[2+], 2 Al(OH)4[-], 3 CO3[2-], 4 HO[-], 26 H2O",
        "log_k": -46.5,
        "molar_volume": 650,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Fe-Ettringite",
        "composition": "6.00 Ca[2+], 2.00 Fe(OH)4[-], 3.00 SO4[2-], 4.00 HO[-], 26.00 H2O",
        "molar_volume": 717,
        "log_k": -44.0,
        "references": ["Moschner2008", "Lothenbach2008"]
    },
    {
        "label": "Thaumasite",
        "composition": "3.00 Ca[2+], SiO(OH)3[-], SO4[2-], CO3[2-], HO[-], 13 H2O",
        "log_k": -24.75,
        "molar_volume": 330,
        "references": ["Matschei2014"]
    },
    {
        "label": "C3AH6",
        "composition": "3 Ca[2+], 2 Al(OH)4[-], 4 HO[-]",
        "log_k": -20.50,
        "molar_volume": 150,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "C3ASO_41H5_18",
        "composition": "3 Ca[2+], 2 Al(OH)4[-], 0.41 SiO(OH)3[-], 3.59 HO[-], -1.23 H2O",
        "log_k": -25.35,
        "molar_volume": 146,
        "references": ["Dilnesa2014a"]
    },
    {
        "label": "C3ASO_84H4_32",
        "composition": "3 Ca[2+], 2 Al(OH)4[-], 0.84 SiO(OH)3[-], 3.16 HO[-], -2.52 H2O",
        "log_k": -26.70,
        "molar_volume": 142,
        "references": ["Dilnesa2014a"]
    },
    {
        "label": "C3FH6",
        "composition": "3.00 Ca[2+], 2.00 Fe(OH)4[-], 4.00 HO[-]",
       "log_k": -26.30,
        "molar_volume": 155,
        "references": ["Dilnesa2014a"]
    },
    {
        "label": "C3FSO_84H4_32",
        "composition": "3 Ca[2+], 2 Fe(OH)4[-], 0.84 SiO(OH)3[-], 3.16 HO[-], -2.52 H2O",
        "log_k": -32.50,
        "molar_volume": 149,
        "references": ["Dilnesa2014a"]
    },
    {
        "label": "C3FS1_34H3_32",
        "composition": "3 Ca[2+], 2 Al(OH)4[-], 1.34 SiO(OH)3[-], 2.66 HO[-], -4.02 H2O",
        "log_k": -34.20,
        "molar_volume": 142,
        "references": ["Dilnesa2014a"]
    },
    {
        "label": "C4AH19",
        "composition": "4 Ca[2+], 2 Al(OH)4[-], 6 HO[-], 12 H2O",
        "log_k": -25.45,
        "molar_volume": 371,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "C4AH13",
        "composition": "4 Ca[2+], 2 Al(OH)4[-], 6 HO[-], 6 H2O",
        "log_k": -25.00,
        "molar_volume": 274,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "C2AH7_5",
        "composition": "2 Ca[2+], 2 Al(OH)4[-], 2 HO[-], 2.5 H2O",
        "log_k": -13.80,
        "molar_volume": 180,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "CAH10",
        "composition": "Ca[2+], 2 Al(OH)4[-], 6 H2O",
        "log_k": -7.60,
        "molar_volume": 193,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "Monosulfoaluminate",
        "composition": "4 Ca[2+], 2 Al(OH)4[-], SO4[2-], 4 HO[-], 6 H2O",
        "log_k": -29.26,
        "molar_volume": 309,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Monocarboaluminate",
        "composition": "4 Ca[2+], 2 Al(OH)4[-], CO3[2-], 4 HO[-], 5 H2O",
        "log_k": -31.47,
        "molar_volume": 262,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Hemicarboaluminate",
        "composition": "4 Ca[2+], 2 Al(OH)4[-], 0.5 CO3[2-], 5 HO[-], 5.5 H2O",
        "log_k": -29.13,
        "molar_volume":  285,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Straetlingite",
        "composition": "2 Ca[2+], 2 Al(OH)4[-], SiO(OH)3[-], HO[-], 2 H2O",
        "log_k": -19.70,
        "molar_volume": 216,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Friedels_salt",
        "composition": "4.0 Ca[2+], 2.0 Al(OH)4[-], 2.0 Cl[-], 4.0 HO[-], 4.0 H2O",
        "log_k": -27.27,
        "molar_volume": 272,
        "references": ["Balonis2010"]
    },
    {
        "label": "Kuzels_salt",
        "composition": "4.00 Ca[2+], 2.00 Al(OH)4[-], Cl[-], 0.5 SO4[2-], 4.0 HO[-], 6.0 H2O",
        "log_k": -28.53,
        "molar_volume": 289,
        "references": ["Balonis2010"]
    },
    {
        "label": "C4FH13",
        "composition": "4.00 Ca[2+], 2.00 Fe(OH)4[-], 6.00 HO[-], 6.00 H2O",
       "log_k": -30.75,
        "molar_volume": 286.00,
        "references": ["Lothenbach2008"]
    },
    {
        "label": "Fe-Monosulfate",
        "composition": "4 Ca[2+], 2 Fe(OH)4[-], SO4[2-], 4 HO[-], 6 H2O",
        "log_k": -31.57,
        "molar_volume": 321,
        "references": ["Dilnesa2012"]
    },
    {
        "label": "Fe-Monocarbonate",
        "composition": "4 Ca[2+], 2 Fe(OH)4[-], CO3[2-], 4 HO[-], 6 H2O",
        "log_k": -34.59,
        "molar_volume": 292,
        "references": ["Dilnesa2011"]
    },
    {
        "label": "Fe-Hemicarbonate",
        "composition": "4 Ca[2+], 2 Fe(OH)4[-], 0.5 CO3[2-], 5 HO[-], 3.5 H2O",
        "log_k": -30.83,
        "molar_volume":  273,
        "references": ["Dilnesa2011"]
    },
    {
        "label": "M4AH10",
        "composition": "4.00 Mg[2+], 2.00 Al(OH)4[-], 6.00 HO[-], 3.00 H2O",
        "log_k": -56.02,
        "molar_volume": 220,
        "references": ["Lothenbach2008", "Lothenbach2006"]
    },
    {
        "label": "0_5_M6ACbH13",
        "composition": "3.00 Mg[2+], Al(OH)4[-], 0.5 CO3[2-], 4.00 HO[-], 2.50 H2O",
        "log_k": -33.29,
        "molar_volume": 115,
        "references": ["Rozov2011"]
    },
    {
        "label": "0_5_M6FCbH13",
        "composition": "3.00 Mg[2+], Fe(OH)4[-], 0.5 CO3[2-], 4.00 HO[-], 2.50 H2O",
        "log_k": -33.64,
        "molar_volume": 119,
        "references": ["Rozov2011"]
    },
    {
        "label": "Anhydrite",
        "composition": "Ca[2+], SO4[2-]",
        "log_k": -4.3575,
        "flag_kinetic": 1,
        "molar_volume": 46,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "Gypsum",
        "composition": "Ca[2+], SO4[2-], 2 H2O",
        "log_k": -4.5809,
        "molar_volume": 74,
        "references": ["Lothenbach2008","Matschei2007"]
    },
    {
        "label": "hemihydrate",
        "composition": "Ca[2+], SO4[2-], 0.5 H2O",
        "log_k": -3.59,
        "molar_volume": 62,
        "flag_kinetic": 1,
        "references": ["Garvin1987"]
    },
    {
        "label": "Syngenite",
        "composition": "2.00 K[+], Ca[2+], 2.00 SO4[2-], H2O",
        "log_k": -7.20,
        "molar_volume": 128,
        "references": ["Lothenbach2006", "Corazza1967"]
    },
    {
        "label": "Gibbsite",
        "composition": "Al(OH)4[-], -HO[-]",
        "log_k": -1.12,
        "molar_volume": 32,
        "flag_kinetic": 1,
        "references": ["Hummel2002"]
    },
    {
        "label": "Al(OH)3(mic)",
        "composition": "Al(OH)4[-], -HO[-]",
        "log_k": -0.67,
        "molar_volume": 32,
        "references": ["Lothenbach2012"]
    },
    {
        "label": "FeOOH(mic)",
        "composition": "Fe(OH)4[-], -HO[-], -2.0 H2O",
        "log_k": -5.6,
        "molar_volume": 21,
        "references": ["Dilnesa2014"]
    },
    {
        "label": "Portlandite",
        "composition": "Ca[2+], +2 HO[-]",
        "log_k": -5.2,
        "molar_volume": 33,
        "references": ["Hummel2002"]
    },
    {
        "label": "SiO2(am)",
        "composition": "SiO(OH)3[-], - H2O, - HO[-]",
        "log_k": 1.476,
        "molar_volume": 29,
        "references": ["Lothenbach2008"]
    },

        // Hydrates

        {
            "label": "CSH,jennite",
            "composition": "1.6667 Ca[2+], 2.3334 HO[-], SiO(OH)3[-], -0.5567 H2O",
            "log_k": -13.17,
            "molar_volume": 78,
            "references": ["Lothenbach2008"]
        },
        {
            "label": "CSH,tobermorite",
            "composition": "0.8333 Ca[2+], 0.6666 HO[-], SiO(OH)3[-], -0.5 H2O",
            "log_k": -8.00,
            "molar_volume": 59,
            "references": ["Lothenbach2008"]
        },


        {
            "label": "Goethite",
            "composition": "Fe[3+], -3.00 H[+], 2.00 H2O",
            "log_k": -1.0000,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Melanterite",
            "composition": "Fe[2+], SO4[2-], 7.00 H2O",
            "log_k": -2.2093,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Brucite",
            "composition": "Mg[2+], 2.00 H2O, -2.00 H[+]",
            "log_k": 16.84,
            "molar_volume": 25,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        // Anhydrous
        {
            "label": "Calcite",
            "composition": "Ca[2+], HCO3[-], -H[+]",
            "log_k": 1.8490,
            "molar_volume": 37,
            "references": ["Hummel2002"]
        },
        // Others
        {
            "label": "Fe(cr)",
            "composition": "Fe[2+], 2.00 E[-]",
            "log_k": 13.8229,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Siderite",
            "composition": "Fe[2+], HCO3[-], -H[+]",
            "log_k": -0.5612,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "FeCO3(pr)",
            "composition": "Fe[2+], HCO3[-], -H[+]",
            "log_k": -0.1211,
            "references": ["Hummel2002"]
        },
        {
            "label": "Hematite",
            "composition": "2.00 Fe[3+], 3.00 H2O, -6.00 H[+]",
            "log_k": 1.1200,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Pyrite",
            "composition": "Fe[2+], 2.00 HS[-], -2.00 H[+], -2.00 E[-]",
            "log_k": -18.500,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Troilite",
            "composition": "Fe[2+], 1.00 HS[-], -1.00 H[+]",
            "log_k": -5.3100,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Magnetite",
            "composition": "Fe[2+], 4.00 H2O, -8.00 H[+], 2.00 Fe[3+]",
            "log_k": 10.0200,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Kaolinite",
            "composition": "2 Al[3+], 2 Si(OH)4, H2O, -6 H[+]",
            "log_k": 7.4350,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "Gibbsite",
            "composition": "Al[3+], 3 H2O, -3 H[+]",
            "log_k": 7.7561,
            "flag_kinetic": 1,
            "references": ["Hummel2002"]
        },
        {
            "label": "C3S",
            "composition": "3 Ca[2+], SiO(OH)3[-], H2O, -5H[+]",
            "log_k": -22.0,
            "flag_kinetic": 1,
            "molar_volume": 73,
            "references": ["Nicoleau2013"]
        },
        {
            "label": "C2S",
            "composition": "2 Ca[2+], SiO(OH)3[-], -3H[+]",
            "log_k": -17.4,
            "molar_volume": 52,
            "flag_kinetic": 1,
            "references": ["Nicoleau2013"]
        },
        {
            "label": "C3A",
            "composition": "3 Ca[2+], 4 HO[-], -6 H2O, 2 Al(OH)4[-]",
            "log_k": 50,
            "molar_volume": 89,
            "flag_kinetic": 1
        },
        {
            "label": "C4AF",
            "composition": "4.00 Ca[2+], 2.00 Fe(OH)4[-], 2.00 Al(OH)4[-], 4.00 HO[-], -10.00 H2O",
            "log_k": 100,
            "molar_volume": 130,
            "flag_kinetic": 1
        },
        // oxyde
        {
            "label": "CaO(ox)",
            "composition": "Ca[2+], H2O, -2.00 H[+]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "MgO(ox)",
            "composition": "Mg[2+], H2O, -2.00 H[+]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "SiO2(ox)",
            "composition": "Si(OH)4, -2.00 H2O",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "Al2O3(ox)",
            "composition": "2.00 Al[3+], 3 H2O, -6.00 H[+]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "SO3(ox)",
            "composition": "SO4[2-], -H2O, +2 H[+]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "Fe2O3(ox)",
            "composition": "2.00 Fe[3+], 3 H2O, -6.00 H[+]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "Na2O(ox)",
            "composition": "2.00 Na[+], - H2O, 2.00 HO[-]",
            "log_k": 50,
            "flag_kinetic": 1
        },
        {
            "label": "K2O(ox)",
            "composition": "2.00 K[+], - H2O, 2.00 HO[-]",
            "log_k": 50,
            "flag_kinetic": 1
        }
    ],
    "Gas" : [
        {
            "label": "CO2(g)",
            "composition": "CO2",
            "log_k": -1.468
        },
        {
            "label": "N2(g)",
            "composition": "N2(aq)",
            "log_k": -3.1864
        },
        {
            "label": "O2(g)",
            "composition": "O2(aq)",
            "log_k": -2.8944
        },
        {
            "label": "H2(g)",
            "composition": "H2(aq)",
            "log_k": -3.1056
        },
        {
            "label": "H2S(g)",
            "composition": "H2S(aq)",
            "log_k": -8.0100
        }
    ],
    "Compounds" : [
        {
            "label": "NaCl",
            "composition": "Na[+], Cl[-]"
        },
        {
            "label": "NaOH",
            "composition": "Na[+], HO[-]"
        },
        {
            "label": "NaNO3",
            "composition": "Na[+], NO3[-]"
        },
        {
            "label": "H2CO3",
            "composition": "HCO3[-], H[+]"
        },
        {
            "label": "KCl",
            "composition": "K[+], Cl[-]"
        },
        {
            "label": "KOH",
            "composition": "K[+], HO[-]"
        },
        {
            "label": "KNO3",
            "composition": "K[+], NO3[-]"
        },
        {
            "label": "CaCl2",
            "composition": "Ca[2+], 2 Cl[-]"
        },
        {
            "label": "MgCl2",
            "composition": "Mg[2+], 2 Cl[-]"
        },
        {
            "label": "HCl",
            "composition": "H[+], Cl[-]"
        }
    ],
    "References" : [
        {
            "key": "Hummel2002",
            "description": "Hummel W., Berner U., Curti E., Pearson F.J. & Thoenen T. (2002): Nagra/PSI Chemical Thermodynamic Data Base 01/01. Universal Publishers/uPUBLISH.com USA, available from: http://www.upublish.com/books/hummel.htm. Also issued as Nagra Technical Report NTB 02-16, Nagra, Wettingen, Switzerland."
        },
        {
            "key": "Lothenbach2008",
            "description": "Lothenbach, Barbara, Matschei, Thomas, Möschner, Göril, Glasser, Fred P.: Thermodynamic modelling of the effect of temperature on the hydration and porosity of Portland cement, Cement and Concrete Research 38(1), 1–18, 2008"
        },
        {
            "key": "Matschei2007",
            "description": "Matschei, Thomas, Lothenbach, Barbara, Glasser, Fredrik P.: Thermodynamic properties of Portland cement hydrates in the system CaO–Al2O3–SiO2–CaSO4–CaCO3–H2O, Cement and Concrete Research 37(10), 1379–1410, 2007"
        },
        {
            "key": "Möschner, G., Lothenbach, B., Rose, J., Ulrich, A., Figi, R., Kretzschmar R. (2008) Solubility of Fe-ettringite (Ca6[Fe(OH)6]2(SO4)3.26H2O), Geochimica et Cosmochimica Acta 72(1), 1-18."
        },
        {
            "key": "Schmidt2008",
            "description": "Schmidt, Thomas, Lothenbach, Barbara, Romer, Michael, Scrivener, Karen, Rentsch, Daniel, Figi, Renato: A thermodynamic and experimental study of the conditions of thaumasite formation, Cement and Concrete Research 38(3), 337–349, 2008"
        },
        {
            "key": "Pearson1991",
            "description": "Pearson F.J. & Berner U. (1991): Nagra Thermochemical Data Base I. Core Data. Nagra Technical Report NTB 91-17, Nagra, Wettingen, Switzerland."
        },
        {
            "key": "Pearson1992",
            "description": " Pearson F.J., Berner U. & Hummel W. (1992): Nagra Thermochemical Data Base II. Supplemental Data 05/92. Nagra Technical Report NTB 91-18, Nagra, Wettingen, Switzerland."
        },
        {
            "key": "Nicoleau2013",
            "description": "Nicoleau, L., Nonat, A., Perrey, D.: The di- and tricalcium silicate dissolutions, Cement and Concrete Research 47(0), 14–30, 2013"
        },
        {
            "key": "Lothenbach2006",
            "description": "Lothenbach, B. and F. Winnefeld (2006), Thermodynamic modelling of the hydration of Portland cement. Cement and Concrete Research 36, 209-226."
        },
        {
            "key": "Corazza1967",
            "description": "Corazza, E., Sabelli, C. (1967) Zeitschrift für Kristallographie 124, 398-408"
        },
        {
            "key": "Matschei2014",
            "description": "T. Matschei and F. Glasser. Thermal stability of thaumasite. Materials and Structures, pages 1--13, 2014."
        },
        {
            "key": "Lothenbach2012",
            "description": "B. Lothenbach, L. Pelletier-Chaignat, and F. Winnefeld. Stability in the system CaO--Al2O3--H2O. Cement and Concrete Research, 42(12):1621--1634, 2012."
        },
        {
            "key": "Dilnesa2014a",
            "description": "B. Z. Dilnesa, B. Lothenbach, G. Renaudin, A. Wichser, and D. Kulik. Synthesis and characterization of hydrogarnet Ca3(AlxFe1-x)2(SiO4)y(OH)4(3−y). Cement and Concrete Research, 59(0):96--111, 2014."
        },
        {
            "key": "Balonis2010",
            "description": "M. Balonis, B. Lothenbach, G. L. Saout, and F. P. Glasser. Impact of chloride on the mineralogy of hydrated Portland cement systems. Cement and Concrete Research, 40(7):1009--1022, 2010."
        },
        {
            "key": "Dilnesa2012",
            "description": "B. Z. Dilnesa, B. Lothenbach, G. Renaudin, A. Wichser, and E. Wieland. Stability of Monosulfate in the Presence of Iron. Journal of the American Ceramic Society, 95(10):3305--3316, 2012."
        },
        {
            "key": "Dilnesa2011",
            "description": "B. Dilnesa, B. Lothenbach, G. L. Saout, G. Renaudin, A. Mesbah, Y. Filinchuk, A. Wichser, and E. Wieland. Iron in carbonate containing {AFm} phases. Cement and Concrete Research, 41(3):311--323, 2011."
        },
        {
            "key": "Rozov2011",
            "description": "K. B. Rozov, U. Berner, D. A. Kulik, and L. W. Diamond. Solubility and thermodynamic properties of carbonate-bearing hydrotalcite-pyroaurite solid solutions with a 3:1 Mg/(Al+Fe) mole ratio. Clays and Clay Minerals, 59(3):215--232, 2011."
        },
        {
            "key": "Garvin1987",
            "description": "D. Garvin, V. Parker, and H. White. CODATA Thermodynamic Tables: Selections for Some Compounds of Calcium and Related Mixtures : a Prototype Set of Tables. CODATA series on thermodynamic properties. Springer Verlag, Berlin, 1987."
        }
    ]
}
