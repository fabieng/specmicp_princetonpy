#!/usr/bin/env python

import unittest as ut

database_path = b"@DATABASE_PATH@/cemdata.js"

import database


class TestDatabase(ut.TestCase):

    def test_init(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.is_valid())
        self.assertTrue(db.nb_component > 0)
        self.assertTrue(db.nb_aqueous > 0)
        self.assertTrue(db.nb_mineral > 0)

    def test_access_aqueous(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.nb_aqueous > 0)
        self.assertTrue(db.nb_aqueous > 2)
        self.assertTrue(db.nb_component > 3)
        label = db.aqueous_id_to_label(2)
        self.assertEqual(db.aqueous_label_to_id(label), 2)
        self.assertIsNotNone(db.nu_aqueous(2, 2))
        self.assertEqual(db.l_nu_aqueous(label, db.component_id_to_label(2)),
                         db.nu_aqueous(2, 2))
        self.assertIsNotNone(db.nu_aqueous(2, 3))
        self.assertEqual(db.l_nu_aqueous(label, db.component_id_to_label(3)),
                         db.nu_aqueous(2, 3))

    def test_access_mineral(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.nb_mineral > 0)
        self.assertTrue(db.nb_mineral > 2)
        self.assertTrue(db.nb_component > 3)
        label = db.mineral_id_to_label(2)
        self.assertEqual(db.mineral_label_to_id(label), 2)
        self.assertIsNotNone(db.nu_mineral(2, 2))
        self.assertEqual(db.l_nu_mineral(label, db.component_id_to_label(2)),
                         db.nu_mineral(2, 2))
        self.assertIsNotNone(db.nu_mineral(2, 3))
        self.assertEqual(db.l_nu_mineral(label, db.component_id_to_label(3)),
                         db.nu_mineral(2, 3))

    def test_switch_basis(self):
        db = database.DatabaseManager(database_path)
        swapping = {b"H[+]": b"HO[-]",
                    b"Al[3+]": b"Al(OH)4[-]"
                   }
        db.swap_components(swapping)
        for (key, value) in swapping.items():
            self.assertRaises(ValueError, db.component_label_to_id, key)
            self.assertNotEqual(db.aqueous_label_to_id(key), -1)
            self.assertNotEqual(db.component_label_to_id(value), -1)
            self.assertRaises(ValueError, db.aqueous_label_to_id, value)

    def test_mineral_keep_only(self):
        db = database.DatabaseManager(database_path)
        mineral_to_keep = (b"Portlandite", b"Gypsum", b"Straetlingite")
        logks = [db.l_logk_mineral(mineral) for mineral in mineral_to_keep]
        db.minerals_keep_only(mineral_to_keep)
        self.assertEqual(db.nb_mineral, 3)
        for i in range(db.nb_mineral):
            self.assertTrue(db.mineral_id_to_label(i) in mineral_to_keep)
            self.assertTrue(db.logk_mineral(i) in logks)

    def test_remove_mineral(self):
        db = database.DatabaseManager(database_path)
        db.remove_solid_phases()
        self.assertEqual(db.nb_mineral, 0)
        self.assertEqual(db.nb_mineral_kinetic, 0)

    def test_remove_gas(self):
        db = database.DatabaseManager(database_path)
        db.remove_gas_phases()
        self.assertEqual(db.nb_gas, 0)


if __name__ == '__main__':
    ut.main()

