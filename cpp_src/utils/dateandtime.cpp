#include "dateandtime.hpp"

#include <string>

namespace specmicp {
namespace dateandtime {


std::string to_text(std::time_t& time_point)
{
    struct std::tm* timeinfo;
    char buffer[80];

    timeinfo = std::localtime(&time_point);

    std::strftime(buffer, 80, "%F - %T", timeinfo);

    return std::string(buffer);
}

std::string to_text_localized(std::time_t& time_point)
{
    struct std::tm* timeinfo;
    char buffer[80];

    timeinfo = std::localtime(&time_point);

    std::strftime(buffer, 80, "%x - %X", timeinfo);

    return std::string(buffer);
}

std::string now_localized()
{
    std::time_t rawtime;

    std::time(&rawtime);
    return to_text_localized(rawtime);
}

std::string now()
{
    std::time_t rawtime;

    std::time(&rawtime);
    return to_text(rawtime);
}

} //end namespace dateandtime
} //end namespace specmicp
