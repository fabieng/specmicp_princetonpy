/*-------------------------------------------------------------------------------

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_UTILS_JSON
#define SPECMICP_UTILS_JSON

//! \file utils/json.hpp
//! \brief Read/Write JSON

#include <memory>
#include "../../3rdparty/jsoncpp/json/json-forwards.h"

namespace specmicp {
//! \namespace specmicp::json
//! \brief JSON helper functions
namespace json {

//! \brief Parse a JSON file
void parse_json(const std::string& filename, Json::Value& root);
//! \brief Parse a JSON input
void parse_json(std::istream& input, Json::Value& root);

//! \brief Save a JSON tree into a file
void save_json(const std::string& filename, const Json::Value& root);
//! \brief Save a JSON tree into a stream
void save_json(std::ostream& output, const Json::Value& root);
////! \brief Save a JSON tree into a string
//void save_json(std::string& output, const Json::Value& root);

//! \brief Raise an error if root[key] does not exist
void check_for_mandatory_member(const Json::Value& root, const std::string& key);
//! \brief Return root[key] raise an error if it does not exist
Json::Value& get_mandatory_member(Json::Value& root, const std::string& key);

} //end namespace json
} //end namespace specmicp

#endif // SPECMICP_UTILS_JSON
