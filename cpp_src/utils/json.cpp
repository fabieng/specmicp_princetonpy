/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "json.hpp"
#include "../../3rdparty/jsoncpp/json/json.h"
#include <fstream>

namespace specmicp {
namespace json {

void parse_json(const std::string& filename, Json::Value& root)
{
    std::ifstream input(filename);
    return parse_json(input, root);
}

void parse_json(std::istream& input, Json::Value& root)
{
    Json::Reader reader;

    bool parsingSuccessful = reader.parse(input, root);
    if ( !parsingSuccessful )
    {
        // report to the user the failure and their locations in the document.
        const std::string message("Failed to parse Json input :\n" + reader.getFormattedErrorMessages());
        throw std::runtime_error(message);
    }
}

void save_json(const std::string& filename, const Json::Value& root)
{
    std::ofstream output(filename);
    save_json(output, root);
    output.close();
}

void save_json(std::ostream& output, const Json::Value& root)
{
    Json::StreamWriterBuilder builder;
    builder["commentStyle"] = "None";
    builder["indentation"] = "   ";
    std::unique_ptr<Json::StreamWriter> writer(
        builder.newStreamWriter());
    writer->write(root, &output);
}

//void save_json(std::string& output, const Json::Value& root)
//{
//    Json::StreamWriterBuilder builder;
//    builder["commentStyle"] = "None";
//    builder["indentation"] = "   ";
//    std::unique_ptr<Json::StreamWriter> writer(
//        builder.newStreamWriter());
//    output.clear();
//    std::ostringstream output_stream(output);
//    writer->write(root, &output_stream);
//}


void check_for_mandatory_member(const Json::Value& root, const std::string& key)
{
    if (not root.isMember(key))
    {
        throw std::invalid_argument(
                    "Missing required attribute in JSON tree : '" + key + "'.");
    }
}

Json::Value& get_mandatory_member(Json::Value& root, const std::string& key)
{
    json::check_for_mandatory_member(root, key);
    return root[key];
}

} //end namespace json
} //end namespace specmicp
