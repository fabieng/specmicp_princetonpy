/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_UTILS_LOG_HPP
#define SPECMICP_UTILS_LOG_HPP

#include <sstream>

//! \file log.hpp logger

namespace specmicp {

//! \namespace specmicp::logger
//! \brief The logger used by SpecMiCP/ReactMiCP
namespace logger {
//! \enum LogLevel the different log level
//!
//!  If changed, the function to_string must also be changed
enum LogLevel {Critical, //!< Error that should lead to abortion
               Error,    //!< Error
               Warning,  //!< Warnings
               Info,     //!< May be worth mentionning, not worth listening...
               Debug,    //!< (Relevant) Debugging information
               Spam      //!< How is your life ? ususally contains large debugging information such as the variables vector
              };

//!  \brief Format the log level into a string
inline std::string to_string(LogLevel);

/*! \brief A log message
 *
 *  The message is written during the destruction of the Log object.
 *  The stream which contain the message is accessed with the member get(Level)
 */
template <typename outputPolicy>
class Log
{
public:
    //! Constructor
    Log() {}
    //! Destructor - Output the message
    ~Log();

    //! \brief Return the steam so we can write the message
    std::ostringstream& get(LogLevel level);

    //! \brief Return the report level
    static LogLevel& ReportLevel(){
        static LogLevel report_level = Debug;
        return report_level;
    }
protected:
    std::ostringstream msg; //!< the actual message
private:
    // this are hidden on purpose, no need
    Log(Log&);
    Log& operator=(Log&);
};

template <typename outputPolicy>
std::ostringstream& Log<outputPolicy>::get(LogLevel level)
{
    msg << to_string(level) << " : ";
    return msg;
}

template <typename outputPolicy>
Log<outputPolicy>::~Log()
{
    outputPolicy::output(msg.str());
}

//! \brief Output Policy to use for logging
class ErrFile
{
public:
    //! \brief Return a pointer to the stream we want to write in
    static std::ostream*& stream();
    //! \brief Output the message to the stream
    static void output(const std::string& msg);
};

inline std::ostream*& ErrFile::stream()
{
    static std::ostream* stream = nullptr;
    return stream;
}

inline void ErrFile::output(const std::string &msg)
{
    std::ostream* out = stream();
    (*out) << msg << std::endl;
    out->flush();
}


inline std::string to_string(LogLevel level)
{
    static const char* list_level[] = {"CRITICAL", "ERROR", "Warning", "info", "debug", "spam"};
    return list_level[static_cast<int>(level)];
}



} // end namespace logger

//! \brief Standard logger type
using stdlog = logger::Log<logger::ErrFile>;

//! \brief Initialize the logger
//!
//! \param out the output stream
//! \param level the output level
inline void init_logger(std::ostream* out, specmicp::logger::LogLevel level) {
    logger::ErrFile::stream() = out;
    stdlog::ReportLevel() = level;
}

//! \brief Filter logs to stdlog
#define FILTER(level) \
    if (level >  stdlog::ReportLevel() || logger::ErrFile::stream() == nullptr) ;\
    else stdlog().get(level)

//! \brief Spam log to stdlog
#define SPAM FILTER(logger::Spam)

//! \brief Debug log to stdlog
#define DEBUG FILTER(logger::Debug)

//! \brief Info log to stdlog
#ifdef INFO
#undef INFO
#endif
#define INFO FILTER(logger::Info)

//! \brief Warning log to stdlog
#define WARNING FILTER(logger::Warning)

//! \brief Error log to stdlog
#define ERROR FILTER(logger::Error)

//! \brief Critical log to stdlog
#define CRITICAL FILTER(logger::Critical)

} // end namespace specmicp

#endif // SPECMICP_UTILS_LOG_HPP
