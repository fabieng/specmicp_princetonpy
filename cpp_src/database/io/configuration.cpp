/*-------------------------------------------------------------------------------

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "configuration.hpp"
#include "../../utils/io/yaml.hpp"
#include "../database.hpp"

#define S_DATABASE "database"
#define S_DATABASE_A_PATH "path"
#define S_DATABASE_A_SWAP "swap_components"

#define S_DATABASE_A_SWAP_K_IN "in"
#define S_DATABASE_A_SWAP_K_OUT "out"

#define S_DATABASE_A_REMOVE_GAS "remove_gas"
#define S_DATABASE_A_REMOVE_SOLIDS "remove_solids"
#define S_DATABASE_A_REMOVE_SORBED "remove_sorbeds"

#define S_DATABASE_A_EXTRA_GAS "extra_gas"
#define S_DATABASE_A_EXTRA_SOLIDS "extra_solids"
#define S_DATABASE_A_EXTRA_SORBED "extra_sorbeds"

#define S_DATABASE_A_LIST_SOLIDS_TOKEEP "list_solids_to_keep"

namespace specmicp {
namespace io {


void swap_components(const YAML::Node& db_conf, database::Database& db_manager);
void keep_only_minerals(const YAML::Node& db_conf, database::Database& db_manager);

void phases_conf(const YAML::Node& db_conf, database::Database& db_manager);

RawDatabasePtr configure_database(const YAML::Node& conf)
{
    check_mandatory_yaml_node(conf, S_DATABASE, "__main__");
    const YAML::Node& db_conf = conf[S_DATABASE];

    const std::string path = get_yaml_mandatory<std::string>(db_conf, S_DATABASE_A_PATH, S_DATABASE);
    database::Database db_manager(path);

    if (db_conf[S_DATABASE_A_SWAP])
        swap_components(db_conf, db_manager);

    phases_conf(db_conf, db_manager);



    return db_manager.get_database();
}


void swap_components(const YAML::Node &db_conf, database::Database &db_manager)
{
    const YAML::Node& swapper = db_conf[S_DATABASE_A_SWAP];
    std::map<std::string, std::string> switch_map;

    for (auto it: swapper)
    {
        switch_map[it[S_DATABASE_A_SWAP_K_OUT].as<std::string>()] = it[S_DATABASE_A_SWAP_K_IN].as<std::string>();
    }
    db_manager.swap_components(switch_map);
}

void phases_conf(const YAML::Node& db_conf, database::Database& db_manager)
{
    if (db_conf[S_DATABASE_A_REMOVE_GAS] and db_conf[S_DATABASE_A_REMOVE_GAS].as<bool>())
        db_manager.remove_gas_phases();

    if (db_conf[S_DATABASE_A_EXTRA_GAS])
        db_manager.add_gas_phases(db_conf[S_DATABASE_A_EXTRA_GAS].as<std::string>());

    if (db_conf[S_DATABASE_A_LIST_SOLIDS_TOKEEP])
    {
        std::vector<std::string> list;
        list.reserve(db_conf[S_DATABASE_A_LIST_SOLIDS_TOKEEP].size());
        for (auto it: db_conf[S_DATABASE_A_LIST_SOLIDS_TOKEEP])
        {
            list.push_back(it.as<std::string>());
        }
        db_manager.minerals_keep_only(list);
    }

    if (db_conf[S_DATABASE_A_REMOVE_SOLIDS] and db_conf[S_DATABASE_A_REMOVE_SOLIDS].as<bool>())
        db_manager.remove_solid_phases();

    if (db_conf[S_DATABASE_A_EXTRA_SOLIDS])
        db_manager.add_solid_phases(db_conf[S_DATABASE_A_EXTRA_SOLIDS].as<std::string>());

    if (db_conf[S_DATABASE_A_REMOVE_SORBED] and db_conf[S_DATABASE_A_REMOVE_SORBED].as<bool>())
        db_manager.remove_gas_phases();

    if (db_conf[S_DATABASE_A_EXTRA_SORBED])
        db_manager.add_sorbed_species(db_conf[S_DATABASE_A_EXTRA_SORBED].as<std::string>());
}

} //end namespace io
} //end namespace specmicp
