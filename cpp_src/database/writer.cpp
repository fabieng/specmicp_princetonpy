/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "writer.hpp"

#include "../utils/json.hpp"
#include "../../3rdparty/jsoncpp/json/json.h"
#include "section_name.hpp"
#include "../utils/dateandtime.hpp"


namespace specmicp {
namespace database {

std::string format_equation(index_t species, const ReactiveSpeciesList& slist, const ComponentList& basis);

void DatabaseWriter::write(const std::string& filename)
{
    Json::Value root;
    set_json_tree(root);
    json::save_json(filename, root);
}

void DatabaseWriter::set_json_tree(Json::Value& root)
{
    set_metadata(root);
    set_basis(root);
    set_aqueous(root);
    set_minerals(root);
    set_gas(root);
    set_sorbed(root);
}

void DatabaseWriter::set_metadata(Json::Value& root)
{
    Json::Value metadata;
    metadata[INDB_ATTRIBUTE_NAME] = data->metadata.name;
    metadata[INDB_ATTRIBUTE_VERSION] = dateandtime::now();
    metadata[INDB_ATTRIBUTE_PATH] = "N/A";

    root[INDB_SECTION_METADATA] = std::move(metadata);
}

void DatabaseWriter::set_basis(Json::Value& root)
{
    Json::Value root_basis;
    root_basis.resize(data->nb_component());
    for (auto id: data->range_component())
    {
        set_component(id, root_basis);
    }
    root[INDB_SECTION_BASIS] = std::move(root_basis);
}

void DatabaseWriter::set_component(index_t component, Json::Value& root_basis)
{
    Json::Value& component_root = root_basis[static_cast<int>(component)];

    component_root[INDB_ATTRIBUTE_LABEL] = data->get_label_component(component);
    component_root[INDB_ATTRIBUTE_MOLARMASS] = data->molar_mass_basis(component, units::MassUnit::gram);
    if (component >= 2)
    {
        Json::Value activity;
        activity[INDB_ATTRIBUTE_ACTIVITY_A] = data->a_debye_component(component);
        activity[INDB_ATTRIBUTE_ACTIVITY_B] = data->b_debye_component(component);
        component_root[INDB_ATTRIBUTE_ACTIVITY] = std::move(activity);
    }
}

void DatabaseWriter::set_aqueous(Json::Value& root)
{
    Json::Value root_aqueous;
    root_aqueous.resize(data->nb_aqueous());
    for (auto id: data->range_aqueous())
    {
        set_aqueous_species(id, root_aqueous);
    }
    root[INDB_SECTION_AQUEOUS] = std::move(root_aqueous);
}

void DatabaseWriter::set_aqueous_species(index_t aqueous, Json::Value& root_aqueous)
{
    Json::Value& species_root = root_aqueous[static_cast<int>(aqueous)];

    species_root[INDB_ATTRIBUTE_LABEL] = data->get_label_aqueous(aqueous);
    species_root[INDB_ATTRIBUTE_COMPOSITION] = format_equation(aqueous, data->aqueous, data->components);
    species_root[INDB_ATTRIBUTE_LOGK] = data->logk_aqueous(aqueous);

    Json::Value activity;
    activity[INDB_ATTRIBUTE_ACTIVITY_A] = data->a_debye_aqueous(aqueous);
    activity[INDB_ATTRIBUTE_ACTIVITY_B] = data->b_debye_aqueous(aqueous);
    species_root[INDB_ATTRIBUTE_ACTIVITY] = std::move(activity);
}

void DatabaseWriter::set_minerals(Json::Value& root)
{
    Json::Value root_mineral;
    root_mineral.resize(data->nb_mineral()+data->nb_mineral_kinetic());
    for (auto id: data->range_mineral())
    {
        set_mineral(id, root_mineral);
    }
    for (auto id: data->range_mineral_kinetic())
    {
        set_mineral_kinetic(id, root_mineral);
    }
    root[INDB_SECTION_MINERALS] = std::move(root_mineral);
}

void DatabaseWriter::set_mineral(index_t mineral, Json::Value& root_mineral)
{
    Json::Value& species_root = root_mineral[static_cast<int>(mineral)];

    species_root[INDB_ATTRIBUTE_LABEL] = data->get_label_mineral(mineral);
    species_root[INDB_ATTRIBUTE_COMPOSITION] = format_equation(mineral, data->minerals, data->components);
    species_root[INDB_ATTRIBUTE_LOGK] = data->logk_mineral(mineral);

    scalar_t molar_volume = data->molar_volume_mineral(mineral, units::LengthUnit::centimeter);
    if (molar_volume > 0) species_root[INDB_ATTRIBUTE_MOLARVOLUME] = molar_volume;
}


void DatabaseWriter::set_mineral_kinetic(index_t mineral, Json::Value& root_mineral)
{
    Json::Value& species_root = root_mineral[static_cast<int>(mineral+data->nb_mineral())];

    species_root[INDB_ATTRIBUTE_LABEL] = data->get_label_mineral_kinetic(mineral);
    species_root[INDB_ATTRIBUTE_COMPOSITION] = format_equation(mineral, data->minerals_kinetic, data->components);
    species_root[INDB_ATTRIBUTE_LOGK] = data->logk_mineral_kinetic(mineral);

    scalar_t molar_volume = data->molar_volume_mineral_kinetic(mineral, units::LengthUnit::centimeter);
    if (molar_volume > 0) species_root[INDB_ATTRIBUTE_MOLARVOLUME] = molar_volume;

    species_root[INDB_ATTRIBUTE_FLAG_KINETIC] = 1;
}


void DatabaseWriter::set_gas(Json::Value& root)
{
    Json::Value root_gas;
    root_gas.resize(data->nb_gas());
    for (auto id: data->range_gas())
    {
        set_gas_phase(id, root_gas);
    }
    root[INDB_SECTION_GAS] = std::move(root_gas);
}

void DatabaseWriter::set_gas_phase(index_t gas, Json::Value& root_gas)
{
    Json::Value& species_root = root_gas[static_cast<int>(gas)];

    species_root[INDB_ATTRIBUTE_LABEL] = data->get_label_gas(gas);
    species_root[INDB_ATTRIBUTE_COMPOSITION] = format_equation(gas, data->gas, data->components);
    species_root[INDB_ATTRIBUTE_LOGK] = data->logk_gas(gas);
}


void DatabaseWriter::set_sorbed(Json::Value& root)
{
    Json::Value root_sorbed;
    root_sorbed.resize(data->nb_sorbed());
    for (auto id: data->range_sorbed())
    {
        set_sorbed_species(id, root_sorbed);
    }
    root[INDB_SECTION_SORBED] = std::move(root_sorbed);
}

void DatabaseWriter::set_sorbed_species(index_t sorbed, Json::Value& root_sorbed)
{
    Json::Value& species_root = root_sorbed[static_cast<int>(sorbed)];

    species_root[INDB_ATTRIBUTE_LABEL] = data->get_label_sorbed(sorbed);
    species_root[INDB_ATTRIBUTE_COMPOSITION] = format_equation(sorbed, data->sorbed, data->components);
    species_root[INDB_ATTRIBUTE_LOGK] = data->logk_sorbed(sorbed);
    species_root[INDB_ATTRIBUTE_NBSITEOCCUPIED] = data->nb_sorption_sites(sorbed);
}

std::string format_equation(index_t species, const ReactiveSpeciesList& slist, const ComponentList& basis)
{
    std::string equation;

    for (index_t component: basis.range())
    {
        if (slist.nu_ji(species, component) != 0)
        {
            char buffer[10];
            std::snprintf(buffer, sizeof(buffer), "%.4f", slist.nu_ji(species, component));
            equation += std::string(buffer) + " " + basis.get_label(component) + ", ";
        }
    }
    equation.erase(equation.size()-2, 2); // remove tailing ', '
    return equation;
}

} //end namespace database
} //end namespace specmicp
