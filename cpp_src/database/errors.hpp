/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_DATABASE_ERRORS_HPP
#define SPECMICP_DATABASE_ERRORS_HPP

#include <vector>
#include <Eigen/Core>
#include <stdexcept>

#include "../types.hpp"

//! \file errors.hpp database errors definitions

namespace specmicp {
namespace database {

// Error used when parsing and handling the database
// -------------------------------------------------

//! \brief This error is thrown when a label does not exist
class db_invalid_label: public std::invalid_argument
{
public:
  db_invalid_label(const std::string &msg):
      std::invalid_argument("Unknown label : " + msg)
  {}
};

//! \brief This error is thrown when a bad syntax is detected in the database
class db_invalid_syntax : public std::invalid_argument
{
public:
  db_invalid_syntax (const std::string &msg):
      std::invalid_argument("Invalid syntax : " + msg)
  {}
};

//! \brief This error is thrown when a bad syntax is detected in the database
class db_invalid_data : public std::invalid_argument
{
public:
  db_invalid_data (const std::string &msg):
      std::invalid_argument("Invalid syntax : " + msg)
  {}
};

//! \brief This error is thrown when the database is not as expected
class invalid_database : public std::invalid_argument
{

public:
  invalid_database (const std::string &msg):
    std::invalid_argument("Invalid Database : " + msg)
  {}
};

//! \brief This error is thrown when a non initialised value is wanted
class db_noninitialized_value : public std::runtime_error
{
public:
  db_noninitialized_value (const std::string &value, const std::string &species):
      std::runtime_error("Value was not initialized : " + value +",  for species : "+species+".")
  {}
};

//! \brief This error is raised when the species already exist
class db_species_already_exist: public std::invalid_argument
{
public:
    db_species_already_exist(std::string species, std::string destination):
        std::invalid_argument("The species '" + species + "' already exist in " + destination + ".")
    {}
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_ERRORS_HPP
