/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef DATABASE_READER_H
#define DATABASE_READER_H

//! \file reader.hpp Read the database from a json file

#include "json/json-forwards.h"
#include "module.hpp"
#include <map>

namespace specmicp {
namespace database {

//! \brief Read a json file containing a database
class SPECMICP_DLL_LOCAL DataReader: public DatabaseModule
{
public:
    //! \brief Empty constructor
    DataReader() {}

    //! \brief Constructor
    DataReader(RawDatabasePtr data):
        DatabaseModule(data)
    {}

    //! \brief Constructor
    //!
    //! @param filepath string containing the path to the database
    DataReader(const std::string& filepath):
        DatabaseModule()
    {
        parse(filepath);
    }
    //! \brief Constructor
    //!
    //! @param input input stream that contains the database
    DataReader(std::istream& input):
        DatabaseModule()
    {
        parse(input);
    }

    //! Return the databes
    RawDatabasePtr get_database() {return data;}

    //! \brief Parse the basis section
    //!
    //! Contains the list of primary species
    void parse_basis(Json::Value& basis_root);
    //! \brief Parse the aqueous section
    //!
    //! Contains the list of secondary species
    void parse_aqueous(Json::Value& aqueous_root, AqueousList& alist);
    //! \brief Parse the mineral section
    //!
    //! Contains the list of minerals
    void parse_minerals(
            Json::Value& minerals,
            MineralList &minerals_list,
            MineralList &minerals_kinetic_list
            );
    //! \brief Parse the gas section
    void parse_gas(Json::Value& gas_root, GasList &glist);
    //! \brief Parse the sorbed species section
    void parse_sorbed(Json::Value& sorbed_root, SorbedList &slist);
    //! \brief Parse the compounds
    void parse_compounds(Json::Value& compounds, CompoundList &clist);

private:
    //! \brief Parse database
    //!
    //! Throw db_invalid_syntax if a syntax error was detected.
    //! Throws std::invalid_argument if the path to the database is not correct
    void parse(std::istream& input);

    //! \brief Parse database
    //!
    //! Throw db_invalid_syntax if a syntax error was detected.
    //! Throws std::invalid_argument if the path to the database is not correct
    void parse(const std::string& filepath);

    //! \brief Parse the metadata section
    //!
    //! we don't do much with them for now....
    void parse_metadata(Json::Value& root);

};

//! \brief Parse an equation
void SPECMICP_DLL_LOCAL parse_equation(const std::string &equation,
                    std::map<std::string, scalar_t>& compo);

//! \brief Get the charge of a species by parsing the label
//!
//! Examples :
//!     - neutral -> 0
//!     - neutral[] -> 0
//!     - charge[+] -> +1
//!     - charge[-1] -> -1
//!     - charge[+2] -> +2
//!     - charge[2-] -> -2
double SPECMICP_DLL_LOCAL charge_from_label(const std::string& label);


} // end namespace database
} // end namespace specmicp

#endif // DATABASE_READER_H
