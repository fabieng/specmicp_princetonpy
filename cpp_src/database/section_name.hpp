#define INDB_SECTION_BASIS "Basis"
#define INDB_SECTION_AQUEOUS "Aqueous"
#define INDB_SECTION_MINERALS "Minerals"
#define INDB_SECTION_GAS "Gas"
#define INDB_SECTION_SORBED "Sorbed"
#define INDB_SECTION_COMPOUNDS "Compounds"

#define INDB_ATTRIBUTE_LABEL "label"
#define INDB_ATTRIBUTE_ACTIVITY "activity"
#define INDB_ATTRIBUTE_ACTIVITY_A "a"
#define INDB_ATTRIBUTE_ACTIVITY_B "b"
#define INDB_ATTRIBUTE_MOLARMASS "molar_mass"
#define INDB_ATTRIBUTE_MOLARVOLUME "molar_volume"

#define INDB_ATTRIBUTE_COMPOSITION "composition"
#define INDB_ATTRIBUTE_LOGK "log_k"
#define INDB_ATTRIBUTE_NBSITEOCCUPIED "nb_sites_occupied"

#define INDB_ATTRIBUTE_FLAG_KINETIC "flag_kinetic"

#define INDB_OPEN_DELIMITER_CHARGE '['
#define INDB_CLOSE_DELIMITER_CHARGE ']'

#define INDB_SECTION_METADATA "Metadata"
#define INDB_ATTRIBUTE_NAME "name"
#define INDB_ATTRIBUTE_VERSION "version"
#define INDB_ATTRIBUTE_PATH "path"


#define INDB_ELECTRON "E[-]"
