/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef DATABASE_WRITER
#define DATABASE_WRITER

//! \file database/writer.hpp
//! \brief Write a database on disk

#include "module.hpp"
#include "json/json-forwards.h"

namespace specmicp {
namespace database {

//! \brief Write a database in the JSON format
//!
//! This class can be used to save the database used in a computation.
//! It will save the current state of the database
class SPECMICP_DLL_LOCAL DatabaseWriter: public DatabaseModule
{
public:
    DatabaseWriter(RawDatabasePtr& the_database):
        DatabaseModule(the_database)
    {}

    //! \brief Write the database
    //!
    //! 2 steps : first set the JSON tree then write it
    void write(const std::string& filename);


    //! \brief Format the DB as a JSON tree
    void set_json_tree(Json::Value& root);

private:
    // The following methods set a section of the database
    void set_metadata(Json::Value& root);
    void set_basis(Json::Value& root);
    void set_component(index_t component, Json::Value& root_basis);
    void set_aqueous(Json::Value& root);
    void set_aqueous_species(index_t aqueous, Json::Value& root_aqueous);
    void set_minerals(Json::Value& root);
    void set_mineral(index_t mineral, Json::Value& root_mineral);
    void set_mineral_kinetic(index_t mineral, Json::Value& root_mineral);
    void set_gas(Json::Value& root);
    void set_gas_phase(index_t gas, Json::Value& root_gas);
    void set_sorbed(Json::Value& root);
    void set_sorbed_species(index_t sorbed, Json::Value& root_sorbed);

};

} //end namespace database
} //end namespace specmicp


#endif // DATABASE_WRITER
