/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "selector.hpp"

#include "aqueous_selector.hpp"

namespace specmicp {
namespace database {


void DatabaseSelector::remove_component(const std::vector<index_t>& id_component_to_remove)
{
    analyse_component(id_component_to_remove);
    select_aqueous(id_component_to_remove);
    select_minerals(id_component_to_remove);
    select_minerals_kinetic(id_component_to_remove);
    if (data->nb_gas() != 0) select_gas(id_component_to_remove);
    select_sorbed(id_component_to_remove);
    select_components();
}

void DatabaseSelector::analyse_component(const std::vector<index_t>& id_component_to_remove)
{
    for (auto it: id_component_to_remove)
    {
        if (it == DataContainer::water_index()) {
            throw std::invalid_argument("Water cannot be removed from the database");
        }
        else if (it == DataContainer::electron_index()) {
            throw std::invalid_argument("The electron cannot be removed from the database");
        }
        m_is_component_to_remove[it] = true;
    }
    m_nb_component_to_keep = data->nb_component() - id_component_to_remove.size();
}

void DatabaseSelector::select_secondary(
        ReactiveSpeciesList* toselect,
        const std::vector<index_t>& id_component_to_remove
)
{
    // first we select which aqueous species we should remove
    std::vector<bool> to_remove(toselect->size(), false);
    for (index_t j: toselect->range())
    {
        for (const auto& it: id_component_to_remove)
        {
            if (toselect->nu_ji(j, it) != 0.0)
            {
                to_remove[j] = true;
                break;
            }
        }
    }
    // then we remove them, in two steps
    const auto nb_to_keep = std::count(to_remove.cbegin(), to_remove.cend(), false);
    auto new_j = 0;
    //      1) first we copy data in the beginning of the arrays
    for (index_t j: toselect->range())
    {
        if (to_remove[j])  continue;
        toselect->move_erase(j, new_j, m_is_component_to_remove);
        ++new_j;
    }
    specmicp_assert(new_j == nb_to_keep);
    //      2) then we resize the arrays
    toselect->resize(nb_to_keep, nb_component_to_keep());
    toselect->set_valid();
    // By doing in this order, we avoid bulk copy of arrays
}

void DatabaseSelector::select_aqueous(const std::vector<index_t>& id_component_to_remove)
{
    select_secondary(&(data->aqueous), id_component_to_remove);
}

void DatabaseSelector::select_minerals(const std::vector<index_t>& id_component_to_remove)
{
    select_secondary(&(data->minerals), id_component_to_remove);
}

void DatabaseSelector::select_minerals_kinetic(const std::vector<index_t>& id_component_to_remove)
{
   select_secondary(&(data->minerals_kinetic), id_component_to_remove);
}


void DatabaseSelector::select_gas(const std::vector<index_t>& id_component_to_remove)
{
    select_secondary(&(data->gas), id_component_to_remove);
}

void DatabaseSelector::select_sorbed(const std::vector<index_t>& id_component_to_remove)
{
    select_secondary(&(data->sorbed), id_component_to_remove);
}

void DatabaseSelector::select_components()
{
    uindex_t new_i = 0;
    for (index_t i: data->range_component())
    {
        if (is_component_to_remove(i)) continue;
        data->components.move_erase(i, new_i);
        ++new_i;
    }
    specmicp_assert(new_i == nb_component_to_keep());
    data->components.resize(nb_component_to_keep());
    data->components.set_valid();
}

void DatabaseSelector::keep_only_component(const std::vector<index_t>& id_to_keep)
{
    // First build the list of components to remove
    std::vector<index_t> id_to_remove;
    id_to_remove.reserve(data->nb_component() - id_to_keep.size());
    for (index_t id: data->range_aqueous_component()) // avoid removing H2O and E[-]
    {
        auto search = std::find(id_to_keep.cbegin(), id_to_keep.cend(), id);
        if (search == id_to_keep.end())
            id_to_remove.push_back(id);
    }
    // Then remove them
    remove_component(id_to_remove);
}

void DatabaseSelector::remove_all_gas()
{
    data->gas = GasList(0, data->nb_component());
    data->gas.set_valid();
}

void DatabaseSelector::remove_all_sorbed()
{
    data->sorbed = SorbedList(0, data->nb_component());
    data->sorbed.set_valid();
}

void DatabaseSelector::remove_all_compounds()
{
    data->compounds = CompoundList(0, data->nb_component());
    data->compounds.set_valid();
}


//! \brief Remove some specific aqueous species
void DatabaseSelector::remove_aqueous(const std::vector<index_t>& id_aqueous)
{
    AqueousSelector(data).remove_aqueous(id_aqueous);
}

} // end namespace database
} // end namespace specmicp
