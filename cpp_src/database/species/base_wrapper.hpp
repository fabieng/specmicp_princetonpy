/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_DATABASE_BASEWRAPPER_HPP
#define SPECMICP_DATABASE_BASEWRAPPER_HPP

#include "../../types.hpp"

//! \file base_wrapper.hpp
//!
//! \brief Base wrapper around vectors and matrices
//!
//! These classes encapsulate an Eigen Matrix (or vector)
//!
//! \defgroup database_species

namespace specmicp {
namespace database {

//! \class MatrixSpeciesWrapper
//! \brief A wrapper around a matrix, to be used by a SpeciesList (or a derived class) instance
//!
//! \ingroup database_species Describing the species in the database
class SPECMICP_DLL_PUBLIC MatrixSpeciesWrapper
{
public:
    //! \brief Default constructor
    MatrixSpeciesWrapper() {}
    //! \brief Constructor initializing the matrix
    MatrixSpeciesWrapper(index_t size, index_t nb_cols):
        m_matrix(Matrix::Zero(size, nb_cols))
    {}

    //! \name Size
    // ============
    //! \brief Return and set the size of the matrix
    //! @{
    //! \brief Return the number of rows in the matrix
    index_t size() const {return m_matrix.rows();}
    //! \brief Return the number of columnns in the matrix
    index_t cols() const {return m_matrix.cols();}
    //! \brief Resize the matrix
    //! This is a resize only for the number of rows
    void resize(index_t size) {
        m_matrix.conservativeResize(size, Eigen::NoChange);
    }
    //! \brief Resize the matrix (both rows and colums)
    void resize(index_t rows, index_t cols) {
        m_matrix.conservativeResize(rows, cols);
    }
    // @}

    //! \name Getter
    // =============
    //! \brief Return the values
    //! @{
    //! \brief Return the element ['row', ['col']
    const scalar_t& operator()(index_t row, index_t col) const {
        return m_matrix(row, col);
    }
    //! \brief Return a const reference to the matrix
    const Matrix& get_matrix() const {
        return m_matrix;
    }
    //! \brief Return the row of a matrix
    Eigen::MatrixBase<Matrix>::ConstRowXpr get_row(index_t k) const {
        return m_matrix.row(k);
    }
    //! \brief Return the element ['row','col']
    const scalar_t& get_value(index_t row, index_t col) const {
        return m_matrix(row, col);
    }
    //! @}

    //! \name Setter
    // =============
    //! \brief Set the values
    //! @{
    //! \brief Add 'other' to row 'k'
    template <typename derived>
    void SPECMICP_DLL_LOCAL add_to_row(index_t k, const Eigen::MatrixBase<derived>& other) {
        m_matrix.row(k) += other;
    }
    //! \brief Multiply row 'k' by 'multiplier'
    template <typename derived>
    void SPECMICP_DLL_LOCAL multiply_by(const Eigen::MatrixBase<derived>& multiplier) {
        m_matrix = m_matrix*multiplier;
    }
    //! \brief Set the value of the matrix by copying 'init_matrix'
    template <typename derived>
    void SPECMICP_DLL_LOCAL set_matrix(const Eigen::MatrixBase<derived>& init_matrix) {
        m_matrix = init_matrix;
    }
    //! \brief Set the element ['row','col'] to 'value'
    void SPECMICP_DLL_LOCAL set_value(index_t row, index_t col, scalar_t value) {
        m_matrix(row, col) = value;
    }

    //! \brief Reset row 'k'
    void SPECMICP_DLL_LOCAL reset_row(index_t k) {
        m_matrix.row(k).setZero();
    }

    //! @}

    //! \name Move
    // ===========
    //! \brief Move values inside, and outside, of the matrix
    //! @{
    //! \brief replace imformation of 'new_ind' by those contained in 'old_ind'
    void move_erase(index_t old_ind, index_t new_ind) {
        m_matrix.row(new_ind) = m_matrix.row(old_ind);
    }
    //! \brief replace imformation of 'new_ind' by those contained in 'old_ind'
    void move_erase(index_t old_ind, index_t new_ind, const std::vector<bool>& is_col_to_remove);
    //! \brief Move the row to another matrix
    void move_erase_to(index_t ind, MatrixSpeciesWrapper& other, index_t other_ind) {
        other.m_matrix.row(other_ind) = m_matrix.row(ind);
        m_matrix.row(ind).setZero();
    }
    //! \brief Swap two rows
    void swap(index_t ind, MatrixSpeciesWrapper& other, index_t other_ind) {
        m_matrix.row(ind).swap(other.m_matrix.row(other_ind));
    }
    //! @}

private:
    Matrix m_matrix;
};

//! \class VectorSpeciesWrapper
//! \brief A wrapper around a vector
//!
//! \ingroup database_species
class SPECMICP_DLL_PUBLIC VectorSpeciesWrapper
{
public:
    //! \brief Default constructor
    VectorSpeciesWrapper() {}
    //! \brief Initialise the vector
    //! \param size initial size of the vector
    VectorSpeciesWrapper(index_t size):
        m_vector(Vector::Zero(size))
    {}

    //! \name Size
    // ============
    //! \brief Return and set the size of the vector
    //! @{
    //! \brief Return the size of the vector
    index_t size() const {return m_vector.rows();}
    //! \brief Resize the vector
    void resize(index_t size) {
        m_vector.conservativeResize(size);
    }
    //! @}

    //! \name Getter
    // =============
    //! \brief Return the values
    //! @{
    //! \brief return the value of index k
    const scalar_t& operator()(index_t k) const {
        return m_vector(k);
    }
    //! \brief Return the inner product of the vector and 'other'
    template <typename derived>
    scalar_t dot(const Eigen::MatrixBase<derived>& other) const {
        return m_vector.dot(other);
    }
    //! \brief Return the value of index k
    const scalar_t& get_value(index_t k) const {
        return m_vector(k);
    }
    //! \brief Return the vector
    const Vector& get_vector() const {
        return m_vector;
    }

    //! @}

    //! \name Setter
    // =============
    //! \brief Set the values
    //! @{
    //! \brief add 'vector'
    template<typename derived>
    void SPECMICP_DLL_LOCAL add(const Eigen::MatrixBase<derived>& vector) {
        m_vector += vector;
    }
    //! \brief Multiply the vector by 'multiplier'
    template<typename derived>
    void SPECMICP_DLL_LOCAL multiply_by(const Eigen::MatrixBase<derived>& multiplier) {
        m_vector = m_vector*multiplier;
    }
    //! \brief Set the value at index k
    void SPECMICP_DLL_LOCAL set_value(index_t k, scalar_t value) {
        m_vector(k) = value;
    }
    //! \brief Copy 'vector' to the vector
    template <typename derived>
    void SPECMICP_DLL_LOCAL set_vector(const Eigen::MatrixBase<derived>& vector) {
        m_vector = vector;
    }
    //! \brief Apply a linear transformation to the vector
    template <typename derived>
    void transform(const Eigen::MatrixBase<derived>& transform_matrix) {
        specmicp_assert(transform_matrix.rows() == transform_matrix.cols());
        m_vector = transform_matrix*m_vector;
    }
    //! @}


    //! \name Move
    // ===========
    //! \brief Move values inside, and outside, of the matrix
    //! @{
    //! \brief Move value of old_ind at new_ind
    void move_erase(index_t old_ind, index_t new_ind) {
        m_vector(new_ind) = m_vector(old_ind);
    }
    //! \brief Move row 'ind' to 'other' at row 'other_ind'
    void move_erase_to(index_t ind, VectorSpeciesWrapper& other, index_t other_ind) {
        other.m_vector(other_ind) = m_vector(ind);
        m_vector(ind) = 0;
    }
    //! @}

private:
    Vector m_vector;
};


} // end namespace database
} // end namespace specmicp

#endif //  SPECMICP_DATABASE_BASEWRAPPER_HPP
