/*-------------------------------------------------------------------------------

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_DATABASE_SPECIES_COMPOUNDS_LIST_HPP
#define SPECMICP_DATABASE_SPECIES_COMPOUNDS_LIST_HPP

//! \file compounds_list.hpp
//! \brief List of compounds

#include "../../types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#ifndef SPECMICP_DATABASE_IONICPARAMETERS_HPP
#include "ionic_parameters.hpp"
#endif
#include "aqueous_list.hpp"

namespace specmicp {
namespace database {

//! \class CompoundList
//! \brief The compound species
//!
//! A compound is an aqueous species that will dissociate immediately
//! in the system. They are use to initialize the system but are not included
//! in the equilibrium computation
//!
//! \ingroup database_species
class CompoundList: public ReactiveSpeciesList
{
public:
    //! Initialize an empty list
    CompoundList() {}
    //! Initialize a list of 'siz' aqueous species with 'nb_components' components
    CompoundList(index_t size, index_t nb_components):
        ReactiveSpeciesList(size, nb_components)
    {}

    //! \name Size
    //! \brief Manage the size of the list
    // --------
    //! @{
    //! \brief Resize the list
    void resize(index_t size) override
    {
        ReactiveSpeciesList::resize(size);
    }
    //! \brief Resize the list, adjust the number of components
    void resize(index_t size, index_t nb_components) override
    {
        ReactiveSpeciesList::resize(size, nb_components);
    }
    //! @}

    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, const std::string& label) {
        set_label(k, label);
        set_logk(k, -1e99);
    }
    //! @}


    //! \name Move
    //! \brief Move species inside the list or to other lists
    // ------------
    //! @{
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind) override
    {
        ReactiveSpeciesList::move_erase(old_ind, new_ind);
    }
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind, const std::vector<bool>& is_reactants_to_remove) override
    {
        ReactiveSpeciesList::move_erase(old_ind, new_ind, is_reactants_to_remove);
    }
    //! @}

    //! \brief Add the aqueous species 'other_species' to 'k', to obtain a canonical system
    void canonicalize(
            index_t ind,
            const AqueousList& aqueous,
            index_t aqueous_ind,
            scalar_t coeff
            )
    {
        add_alien_species_to(ind, aqueous, aqueous_ind, coeff);
    }

    //! \brief Append the species in this list to the other list
    void append_to(CompoundList& other);
};


} //end namespace database
} //end namespace specmicp

#endif // SPECMICP_DATABASE_SPECIES_COMPOUNDS_LIST_HPP
