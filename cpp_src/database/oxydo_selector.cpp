/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "oxydo_selector.hpp"

namespace specmicp {
namespace database {

//! \brief Remove the half-cells reaction corresponding to list_components in 'slist'
void remove_half_cells_secondary(
        ReactiveSpeciesList* slist,
        const std::vector<index_t>& list_components);

void OxydoSelector::remove_half_cells(const std::vector<std::string>& list_components)
{
    std::vector<index_t> list_id_components(list_components.size());
    for (auto i=0; i<list_components.size(); ++i)
    {
        list_id_components[i] = safe_component_label_to_id(list_components[i]);
    }
    remove_half_cells(list_id_components);
}

void OxydoSelector::remove_half_cells(const std::vector<index_t>& list_components)
{
    remove_half_cells_secondary(&(data->aqueous), list_components);
    remove_half_cells_secondary(&(data->sorbed), list_components);
    remove_half_cells_secondary(&(data->gas), list_components);
    remove_half_cells_secondary(&(data->minerals), list_components);
    remove_half_cells_secondary(&(data->minerals_kinetic), list_components);
}

//! \brief Remove the half-cells reaction for the aqueous species
void remove_half_cells_secondary(
        ReactiveSpeciesList* slist,
        const std::vector<index_t>& list_components)
{
    // first we select the species to remove
    std::vector<bool> to_remove(slist->size(), false);

    for (auto id: slist->range())
    {
        if (slist->nu_ji(id, DataContainer::electron_index()) == 0.0) continue;
        for (const auto& component: list_components)
        {
            if (slist->nu_ji(id, component) != 0.0)
                to_remove[id] = true;
        }
    }

    // then we remove them, in two steps
    const auto nb_to_keep = std::count(to_remove.cbegin(), to_remove.cend(), false);
    auto new_j = 0;
    //      1) first we copy data in the beginning of the arrays
    for (index_t j: slist->range())
    {
        if (to_remove[j])  continue;
        slist->move_erase(j, new_j);
        ++new_j;
    }
    specmicp_assert(new_j == nb_to_keep);
    //      2) then we resize the arrays
    slist->resize(nb_to_keep);
    slist->set_valid();
}

} // end namespace database
} // end namespace specmicp
