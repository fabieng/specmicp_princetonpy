/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "mineral_selector.hpp"

namespace specmicp {
namespace database {


void MineralSelector::keep_only(const std::vector<index_t>& minerals_to_keep)
{
    // First we compute the new sizes
    const uindex_t nb_to_keep = minerals_to_keep.size();
    const uindex_t nb_to_remove = data->nb_mineral() - nb_to_keep;
    const uindex_t nb_new_min_kin = data->nb_mineral_kinetic() + nb_to_remove;

    uindex_t new_id = 0;
    uindex_t new_id_kin = data->nb_mineral_kinetic();
    // Then we assure that there is place for moving data
    data->minerals_kinetic.resize(nb_new_min_kin);

    // Then we move data
    for (index_t id: data->range_mineral())
    {
        auto search = std::find(minerals_to_keep.begin(), minerals_to_keep.end(), id);
        if (search == minerals_to_keep.end() ) // to move to kinetic
        {
            data->minerals.move_erase_to(id, data->minerals_kinetic, new_id_kin);
            ++new_id_kin;
        }
        else
        {
            data->minerals.move_erase(id, new_id);
            ++new_id;
        }
    }
    specmicp_assert(new_id == nb_to_keep); // simple test to assure that everything is ok
    specmicp_assert(new_id_kin == nb_new_min_kin);
    // Finally we shrink the containers for the mineral at equilibrium
    data->minerals.resize(nb_to_keep);
    data->minerals.set_valid();
    data->minerals_kinetic.set_valid();
}


void MineralSelector::keep_only(const std::vector<std::string>& minerals_to_keep)
{
    std::vector<index_t> ids(minerals_to_keep.size());
    for (uindex_t min=0; min<minerals_to_keep.size(); ++min)
    {
        ids[min] = safe_mineral_label_to_id(minerals_to_keep[min]);
    }
    keep_only(ids);
}


void MineralSelector::remove_all_minerals()
{
    data->minerals = MineralList(0, data->nb_component());
    data->minerals_kinetic = MineralList(0, data->nb_component());

    data->minerals.set_valid();
    data->minerals_kinetic.set_valid();
}

} // end namespace database
} // end namespace specmicp
