/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_TYPES_HPP
#define SPECMICP_TYPES_HPP

#include "macros.hpp"

#include <type_traits>
#include <boost/range/irange.hpp> // boost everywhere \o/

//! \file src/types.hpp
//! \brief types and initialization
//!
//! This file is expected to be included by all other files.
//! It defines the main types used throughout SpecMiCP

// Any change to this file should cause a complete rebuild of the project

// ==========
// Index Type
// ==========

// First, we need to define the type of an index, before including Eigen
//! \namespace specmicp Main namespace for the SpecMiCP solver
namespace specmicp {


#ifndef EIGEN_DEFAULT_DENSE_INDEX_TYPE
using index_t = std::ptrdiff_t; //!< Type of an index in a vector
#else
using index_t = EIGEN_DEFAULT_DENSE_INDEX_TYPE;
#endif

using uindex_t = typename std::make_unsigned<index_t>::type; //!< Unsigned version of the index


}
//! \def EIGEN_DEFAULT_DENSE_INDEX_TYPE
//! \brief The default type for an index in a dense matrix
//!
//! This may be redefined to be consistent.
//! For more information see the Eigen documentation.

// now we can include Eigen
#ifndef EIGEN_DEFAULT_DENSE_INDEX_TYPE
#define EIGEN_DEFAULT_DENSE_INDEX_TYPE specmicp::index_t
#endif

#include <Eigen/Core>

// ============
// Matrix stuff
// ============

namespace specmicp {

using scalar_t = double; //!< Type of a scalar

// linear algebra
//! A dense vector
using Vector = Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>;
//! A dense matrix
using Matrix = Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic>;

// Range
// =====

//! \brief Range
//!
//! used to iterate over species/elements/nodes/...
using range_t = boost::iterator_range<boost::range_detail::integer_iterator<index_t>>;

// constants
// =========

//! \brief Id of an equation that is not an equation
constexpr index_t no_equation = -1;
//! \brief Id of a non-existant species
constexpr index_t no_species = -1;

//! \brief Precision used to compute jacobian
constexpr scalar_t eps_jacobian = 1e-8;

} // namespace specmicp


#endif // SPECMICP_TYPES_HPP
