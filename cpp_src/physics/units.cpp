/*-------------------------------------------------------------------------------

Copyright (c) 2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#include "units.hpp"

#include <algorithm>

namespace specmicp {
namespace units {

scalar_t convert_pressure(scalar_t pressure_si, LengthUnit length_unit)
{
    scalar_t pressure;
    switch (length_unit) {
    case LengthUnit::meter:
        pressure = pressure_si;
        break;
    case LengthUnit::decimeter:
        pressure = 1e-2*pressure_si;
        break;
    case LengthUnit::centimeter:
        pressure = 1e-4*pressure_si;
    }
    return pressure;
}

void parse_amount_unit(std::string unit, AmountUnit& amounts)
{

    auto n = unit.find("/");
    amounts.type = AmountUnitType::Unknown;
    amounts.factor_si = -1;
    scalar_t factor;
    if (n == std::string::npos)
    {
        unit.erase(std::remove(unit.begin(), unit.end(), ' '),
                       unit.end());
        factor = is_mass_unit(unit);
        if (factor > 0)
        {
            amounts.type = AmountUnitType::Mass;
            amounts.factor_si = factor;
            return;
        }
        factor = is_mole_unit(unit);
        if (factor > 0)
        {
            amounts.type = AmountUnitType::NumberOfMoles;
            amounts.factor_si = factor;
            return;
        }
        factor = is_volume_unit(unit);
        if (factor > 0)
        {
            amounts.type = AmountUnitType::Volume;
            amounts.factor_si = factor;
            return;
        }
    }
    else
    {
        // composite unit
        std::string numer = unit.substr(0, n);
        numer.erase(std::remove(numer.begin(), numer.end(), ' '),
                       numer.end());
        std::string denom = unit.substr(n+1, unit.size()-n);
        denom.erase(std::remove(denom.begin(), denom.end(), ' '),
                       denom.end());
        scalar_t factor_numer = -1;
        scalar_t factor_denom = -1;

        factor_denom = is_volume_unit(denom);
        if (factor_denom > 0)
        {
            // concentration
            factor_numer = is_mass_unit(numer);
            if (factor_numer > 0)
            {
                amounts.type = AmountUnitType::MassConcentration;
                amounts.factor_si = factor_numer / factor_denom;
                return;
            }
            factor_numer = is_mole_unit(numer);
            if (factor_numer > 0)
            {
                amounts.type = AmountUnitType::MoleConcentration;
                amounts.factor_si = factor_numer / factor_denom;
                return;
            }
            return;
        }
        factor_denom = is_mass_unit(denom);
        if (factor_denom > 0)
        {
            factor_numer = is_mole_unit(numer);
            if (factor_numer > 0)
            {
                amounts.type = AmountUnitType::Molality;
                amounts.factor_si = factor_numer / factor_denom;
                return;
            }
        }
    }
}

scalar_t is_mass_unit(const std::string& unit)
{
    scalar_t factor = -1;
    if (unit == "kg" or unit == "kilogram")
        factor = 1;
    else if (unit == "g" or unit == "gram")
        factor = 1e-3;
    return factor;
}

scalar_t is_length_unit(const std::string& unit)
{
    scalar_t factor = -1;
    if (unit == "m" or unit == "meter")
        factor = 1;
    else if (unit == "dm" or unit == "decimeter")
        factor = 0.1;
    else if (unit == "cm" or unit == "centimeter")
        factor = 0.01;
    else if (unit == "mm" or unit == "millimeter")
        factor = 1e-3;
    return factor;
}

scalar_t is_volume_unit(const std::string& unit)
{
    scalar_t factor = -1;
    if (unit == "m^3" or unit == "cubicmeter")
        factor = 1;
    else if (unit == "dm^3" or unit == "cubicdecimeter")
        factor = 1e-3;
    else if (unit == "cm^3" or unit == "cubiccentimeter")
        factor = 1e-6;
    else if (unit == "mm^3" or unit == "cubicmillimeter")
        factor = 1e-9;
    return factor;
}

scalar_t is_mole_unit(const std::string &unit)
{
    scalar_t factor = -1;
    if (unit == "mol" or unit == "moles")
        factor = 1;
    else if (unit == "mmol" or unit == "millimoles")
        factor = 1e-3;
    return factor;
}

} //end namespace units
} //end namespace specmicp
