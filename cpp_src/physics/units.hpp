/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_UNITS_UNITS
#define SPECMICP_UNITS_UNITS

#include "../types.hpp"

//! \file physics/units.hpp
//! \brief units management and conversion

namespace specmicp {

//! \namespace specmicp::units
//! \brief Units management
namespace units {

//! units used for length
enum class LengthUnit {
    meter,
    decimeter,
    centimeter
};

//! units used for mass
enum class MassUnit {
    kilogram,
    gram
};


//! \brief Simple struct which contains the unit information
struct UnitsSet
{
    MassUnit mass; //!< The unit to use for mass
    LengthUnit length; //!< The unit to use for length (also area and volume)

    //! \brief Defaults are the SI unit system
    UnitsSet():
        mass(MassUnit::kilogram),
        length(LengthUnit::meter)
    {}
};

//! \brief Base class that handles the units
//!
//!  To be inherited by other classes that need units
class UnitBaseClass
{
public:
    UnitBaseClass() {}
    UnitBaseClass(UnitsSet units):
        m_units(units)
    {}
    //! \brief Return the units
    UnitsSet get_units() {return m_units;}

    //! \brief Set the units
    void set_units(UnitsSet units) {m_units = units;}

    //! \brief Return the Mass unit
    MassUnit mass_unit() const {return m_units.mass;}
    //! \brief Return the Length unit
    LengthUnit length_unit() const {return m_units.length;}

    //! \brief Return the mass unit
    MassUnit& mass_unit() {return m_units.mass;}
    //! \brief Return the length unit
    LengthUnit& length_unit() {return m_units.length;}

private:
    UnitsSet m_units;
};


//! \brief the type of an 'amount' unit
enum class AmountUnitType
{
    Mass,
    Volume,
    NumberOfMoles,
    MoleConcentration,
    MassConcentration,
    Molality,
    Unknown
};

//! \brief Description of an amount unit
struct AmountUnit
{
    AmountUnitType type; //!< The unit type
    scalar_t factor_si; //!< unit*factor_si = SI unit
};

//! \brief Find the unit and factor of 'unit'
//!
//! \param unit the unit to analyse
//! \param amounts
void parse_amount_unit(std::string unit, AmountUnit& amounts);

//! \brief Check if 'unit' is a mass unit
//!
//! \return if it is a mass unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_mass_unit(const std::string& unit);
//! \brief Check if 'unit' is a length unit
//!
//! \return if it is a length unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_length_unit(const std::string& unit);
//! \brief Check if 'unit' is a volume unit
//!
//! \return if it is a volume unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_volume_unit(const std::string& unit);
//! \brief Check if 'unit' is a mole unit
//!
//! \return if it is a mole unit return the factor to transform it to its SI equivalent, else return a negative number
scalar_t is_mole_unit(const std::string& unit);

// Temperature
// -----------

//! Convert a temperature from Celsius to Kelvin
inline constexpr scalar_t celsius(scalar_t tc) {return tc + 273.15;}
//! Convert a temperature from Kelvin to Celsius
inline constexpr scalar_t to_celsius(scalar_t tk) { return tk - 273.15;}

// Volume
// ------

//! Convert liter to cubic meter
inline constexpr scalar_t liter(scalar_t vl) {return 1e-3*vl;}
//! convert cubic meter to liter
inline constexpr scalar_t to_liter(scalar_t vcm) {return 1e3*vcm;}

// Pressure
// --------


scalar_t SPECMICP_DLL_PUBLIC convert_pressure(scalar_t pressure_si, LengthUnit length_unit);

} // end namespace units
} // end namespace specmicp

#endif // SPECMICP_UNITS_UNITS
