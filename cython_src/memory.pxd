# definition from <memory>
#
cdef extern from "<memory>" namespace "std":
    #
    #   The shared ptr
    #       note : there is no overloading of operator->
    #       the true object is accessed through the get() member function
    #
    cdef cppclass shared_ptr[T]:
        shared_ptr()
        shared_ptr(shared_ptr[T])
        T* get()

    shared_ptr[T] make_shared[T]()
