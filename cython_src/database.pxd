#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Cython includes :

# libcpp :

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map as cmap
from libcpp cimport bool
from memory cimport shared_ptr # shared_ptr is missing from cython includes

# eigen

from eigen cimport MatrixXd, VectorXd

# Declarations of the C++ API 
# ============================

# The raw container
# =================

# Note : there is no declaration of a constructor,
# it cannot be build from the cython side.

cdef extern from "specmicp/database/data_container.hpp" namespace "specmicp::database":
    cdef cppclass DataContainer:
        int water_index();
        int electron_index();

        # basis
        # -----
        int nb_component()
        int nb_aqueous_components()
        int get_id_component(string)
        string get_label_component(int)

        # aqueous
        # -------
        int nb_aqueous()
        int get_id_aqueous(string)
        string get_label_aqueous(int)
        double nu_aqueous(int, int)
        double logk_aqueous(int)

        # mineral
        # -------
        int nb_mineral()
        int get_id_mineral(string)
        string get_label_mineral(int)
        double nu_mineral(int, int)
        double logk_mineral(int)
        double molar_mass_mineral(int)
        double molar_volume_mineral(int)

        int nb_mineral_kinetic()
        int get_id_mineral_kinetic(string)
        string get_label_mineral_kinetic(int)
        double nu_mineral_kinetic(int, int)
        double logk_mineral_kinetic(int)
        double molar_mass_mineral_kinetic(int)
        double molar_volume_mineral_kinetic(int)

        # gas
        # ---
        int nb_gas()
        int get_id_gas(string)
        string get_label_gas(int)
        double nu_gas(int, int)
        double logk_gas(int)


# The database manager
# ====================
# Used to create and modify the database

cdef extern from "specmicp/database/database.hpp" namespace "specmicp::database":
    cdef cppclass Database:
        # cython-cpp interface - list of methods that we can access
        # Database should be the only module accessible from python
        Database()
        Database(shared_ptr[DataContainer])
        Database(string) except +
        void parse_database(string) except +
        shared_ptr[DataContainer] get_database()
        void swap_components(cmap[string, string]) except +
        void minerals_keep_only(vector[string]) except +
        bool is_valid()
        void remove_gas_phases()
        void add_gas_phases(string)
        void remove_solid_phases()
        void add_solid_phases(string)
        void remove_sorbed_species()
        void add_sorbed_species(string)
        void save(string)
        # The following methods are from
        # specmicp::database::DatabaseModule
        int component_label_to_id(string)
        int safe_component_label_to_id(string) except +
        int aqueous_label_to_id(string)
        int safe_aqueous_label_to_id(string) except +
        int mineral_label_to_id(string)
        int safe_mineral_label_to_id(string) except +
        int mineral_kinetic_label_to_id(string)
        int safe_mineral_kinetic_label_to_id(string) except +
        int gas_label_to_id(string)
        int safe_gas_label_to_id(string) except +


# Declaration of the Cython Interface : the extension class

cdef class DatabaseManager:
    """ The Python database handler

    Use this class to create a database, to check values in the database
    or switch basis, drop minerals or components...
    """
    # C++ attribute :
    #    - the raw database
    cdef shared_ptr[DataContainer] container
    #    -  the database manager
    cdef Database *database
    
    # the "pure" cython methods
    #      these methods will not be available from python
    cdef void init_database(self, shared_ptr[DataContainer] raw_db)
    cdef shared_ptr[DataContainer] get_raw_db(self)
    cdef DataContainer* _get(self)

    # everything else is declared in the "database.pyx" file