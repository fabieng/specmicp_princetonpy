This is an "extract" of the [SpecMiCP][1] database module created for a
[PrincetonPy][2] tutorial on cython.

[1]: https://bitbucket.org/specmicp/specmicp
[2]: http://princetonpy.com
